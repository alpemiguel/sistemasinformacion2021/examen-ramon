<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciudades".
 *
 * @property int $id
 * @property string $nombre
 * @property float $habitantes
 * @property string $escudo
 * @property string $mapa
 */
class Ciudades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'habitantes', 'escudo', 'mapa'], 'required'],
            [['id'], 'integer'],
            [['habitantes'], 'number'],
            [['nombre', 'escudo', 'mapa'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'habitantes' => 'Habitantes',
            'escudo' => 'Escudo',
            'mapa' => 'Mapa',
        ];
    }
}
